folder\_structure package
=========================

Submodules
----------

folder\_structure.cli module
----------------------------

.. automodule:: folder_structure.cli
   :members:
   :undoc-members:
   :show-inheritance:

folder\_structure.folder module
-------------------------------

.. automodule:: folder_structure.folder
   :members:
   :undoc-members:
   :show-inheritance:

folder\_structure.schema module
-------------------------------

.. automodule:: folder_structure.schema
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: folder_structure
   :members:
   :undoc-members:
   :show-inheritance:
