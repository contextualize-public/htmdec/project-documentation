<!DOCTYPE html>

<html lang="en" data-content_root="./">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" /><meta name="viewport" content="width=device-width, initial-scale=1" />

    <title>birdshot &#8212; birdshot 0.0.post1.dev20+g168d24e.d20231220 documentation</title>
    <link rel="stylesheet" type="text/css" href="_static/pygments.css?v=fa44fd50" />
    <link rel="stylesheet" type="text/css" href="_static/alabaster.css?v=233b9934" />
    <script src="_static/documentation_options.js?v=c5a7b33d"></script>
    <script src="_static/doctools.js?v=888ff710"></script>
    <script src="_static/sphinx_highlight.js?v=dc90522c"></script>
    <link rel="index" title="Index" href="genindex.html" />
    <link rel="search" title="Search" href="search.html" />
    <link rel="next" title="birdshot" href="cli.html" />
    <link rel="prev" title="birdshot" href="index.html" />
   
  <link rel="stylesheet" href="_static/custom.css" type="text/css" />
  
  
  <meta name="viewport" content="width=device-width, initial-scale=0.9, maximum-scale=0.9" />

  </head><body>
  

    <div class="document">
      <div class="documentwrapper">
        <div class="bodywrapper">
          

          <div class="body" role="main">
            
  <section id="birdshot">
<span id="readme"></span><h1>birdshot<a class="headerlink" href="#birdshot" title="Link to this heading">¶</a></h1>
<p>Utilities and functions specific to the BIRDSHOT project.</p>
<p>This package will most frequently be used as a command line tool within the
Carta Workflow framework (powered by SevenBridges). Subcommands to the
<code class="docutils literal notranslate"><span class="pre">birdshot</span></code> CLI create and operate on a stream of objects, similar to a linux
pipe.</p>
<p>Subcommands can be broadly split into two categories: generators and
processors.</p>
<p>Generators add objects into the stream. <code class="docutils literal notranslate"><span class="pre">load</span></code> is an excellent example as it
reads an object from a file and adds that object to the stream. Multiple
<code class="docutils literal notranslate"><span class="pre">load</span></code> commands will add multiple objects to the stream.</p>
<p>Processors act on the objects in the stream and can modify, consume, or let
pass stream objects.</p>
<p>For example, a graph previously downloaded from Google Drive could be loaded
and the nodes filtered to list all JSON files:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="n">birdshot</span> <span class="n">load</span> <span class="s2">&quot;graph.pkl&quot;</span> <span class="n">store</span> <span class="o">-</span><span class="n">k</span> <span class="n">G</span> <span class="n">nodes</span> <span class="n">node</span><span class="o">-</span><span class="nb">filter</span> <span class="o">-</span><span class="n">n</span> <span class="s1">&#39;.*.json&#39;</span> <span class="n">path</span> <span class="o">--</span><span class="n">graph</span> <span class="n">G</span>
</pre></div>
</div>
<p>will return the fully qualified path of all JSON files. Let’s break that down
into the individual parts:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="n">birdshot</span> <span class="n">load</span> <span class="s2">&quot;graph.pkl&quot;</span><span class="o">...</span>
</pre></div>
</div>
<p>This adds the graph stored in “graph.pkl” into the stream. The stream now
contains one object: the graph. As we’ll need it later, store a copy of this
graph:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="o">...</span> <span class="n">store</span> <span class="o">-</span><span class="n">k</span> <span class="n">G</span> <span class="o">...</span>
</pre></div>
</div>
<p>This stores the graph on the heap: a map of named variables. This is
effectively equivalent to creating a key-value pair in a dictionary:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="n">var</span> <span class="o">=</span> <span class="p">{</span><span class="s2">&quot;G&quot;</span><span class="p">:</span> <span class="n">graph</span><span class="p">}</span>
</pre></div>
</div>
<p>And like a dictionary, all the variables must be given unique names. The graph
contains nodes and edges that define the graph structure.</p>
<p>The <code class="docutils literal notranslate"><span class="pre">store</span></code> command does not consume the graph. The graph continues through
the stream. However, we want to work with the nodes in the graph–the nodes
represent files and folders in the Birdshot Google Drive. To access these
nodes is a simple command:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="o">...</span> <span class="n">nodes</span> <span class="o">...</span>
</pre></div>
</div>
<p>This consumes the graph (it is no longer in the stream) and adds all the nodes
to the stream. (You can think about the stream as a list or queue of objects
to be processed.) From there we want to find nodes whose name (<code class="docutils literal notranslate"><span class="pre">-n</span></code>) matches
a regular expression pattern:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="o">...</span> <span class="n">node</span><span class="o">-</span><span class="nb">filter</span> <span class="o">-</span><span class="n">n</span> <span class="s1">&#39;.+\.json&#39;</span> <span class="o">...</span>
</pre></div>
</div>
<p>The <code class="docutils literal notranslate"><span class="pre">-n</span></code> option tests against the node name (e.g., the filename or folder
name). Only matching names are kept. All others are discarded. Alternatively,
one could search on node ID using the <code class="docutils literal notranslate"><span class="pre">--id</span></code> flag.</p>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p><code class="docutils literal notranslate"><span class="pre">node-filter</span></code> matches
<a class="reference external" href="https://docs.python.org/3/library/re.html">regular expressions</a> not
<a class="reference external" href="https://docs.python.org/3/library/glob.html">glob pattern expansion</a>.
Subtle and easy to miss, the pattern above, <code class="docutils literal notranslate"><span class="pre">.+\.json</span></code> matches any file
ending in “.json”. The analogous glob pattern, <code class="docutils literal notranslate"><span class="pre">*.json</span></code>, while simplier
for file names, does not extend to more general pattern matching.</p>
</div>
<p>Once matched, we want to list the path to each JSON file:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="o">...</span> <span class="n">path</span> <span class="o">--</span><span class="n">graph</span> <span class="n">G</span>
</pre></div>
</div>
<p>This takes each node and, using the graph stored on the heap <code class="docutils literal notranslate"><span class="pre">--graph</span> <span class="pre">G</span></code>,
returns the path.</p>
<section id="installation">
<h2>Installation<a class="headerlink" href="#installation" title="Link to this heading">¶</a></h2>
<p>This package relies on <em>pycarta</em> and <em>carta-integrations</em>, neither of which is
available through PyPi and must be installed through wheels. These two packages
and their dependencies are provided in the <em>wheelhouse</em> folder and should be
installed before pip-installing birdshot.</p>
<p>Start by compiling the wheels for the local packages in a <em>clean/fresh virtual environment</em>:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="n">python</span> <span class="o">-</span><span class="n">m</span> <span class="n">pip</span> <span class="n">wheel</span> <span class="o">-</span><span class="n">w</span> <span class="n">wheelhouse</span> <span class="o">/</span><span class="n">path</span><span class="o">/</span><span class="n">to</span><span class="o">/</span><span class="n">carta</span><span class="o">/</span><span class="n">carta</span><span class="o">-</span><span class="n">integrations</span>
<span class="n">python</span> <span class="o">-</span><span class="n">m</span> <span class="n">pip</span> <span class="n">install</span> <span class="o">--</span><span class="n">no</span><span class="o">-</span><span class="n">index</span> <span class="o">--</span><span class="n">find</span><span class="o">-</span><span class="n">links</span><span class="o">=</span><span class="n">wheelhouse</span> <span class="o">--</span><span class="n">force</span><span class="o">-</span><span class="n">reinstall</span> <span class="n">carta</span><span class="o">-</span><span class="n">integrations</span>
</pre></div>
</div>
<p>and, similarly, for pycarta:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="n">python</span> <span class="o">-</span><span class="n">m</span> <span class="n">pip</span> <span class="n">wheel</span> <span class="o">-</span><span class="n">w</span> <span class="n">wheelhouse</span> <span class="o">/</span><span class="n">path</span><span class="o">/</span><span class="n">to</span><span class="o">/</span><span class="n">pycarta</span>
<span class="n">python</span> <span class="o">-</span><span class="n">m</span> <span class="n">pip</span> <span class="n">install</span> <span class="o">--</span><span class="n">no</span><span class="o">-</span><span class="n">index</span> <span class="o">--</span><span class="n">find</span><span class="o">-</span><span class="n">links</span><span class="o">=</span><span class="n">wheelhouse</span> <span class="o">--</span><span class="n">force</span><span class="o">-</span><span class="n">reinstall</span> <span class="n">pycarta</span>
</pre></div>
</div>
<p>After this, install birdshot: <code class="docutils literal notranslate"><span class="pre">pip</span> <span class="pre">install</span> <span class="pre">.</span></code></p>
<p>It may be necessary to deactivate/reactivate the virtual environment before the
<code class="docutils literal notranslate"><span class="pre">birdshot</span></code> CLI is accessible.</p>
<section id="building-the-docker-container">
<h3>Building the Docker Container<a class="headerlink" href="#building-the-docker-container" title="Link to this heading">¶</a></h3>
<p>Once the package is built in a clean virtual environment:</p>
<ol class="arabic simple">
<li><p>Delete all wheels in <em>wheelhouse/</em> except <em>carta-integrations</em> and <em>pycarta</em>.</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">pip</span> <span class="pre">freeze</span> <span class="pre">|</span> <span class="pre">sed</span> <span class="pre">-e</span> <span class="pre">'s/=.*//'</span> <span class="pre">&gt;</span> <span class="pre">requirements.txt</span></code></p></li>
<li><p>Edit the <em>requirements.txt</em> to remove birdshot, carta-integrations, and pycarta.</p></li>
<li><p><cite>docker buildx build –platform linux/x86_64 -t images.sbgenomics.com/&lt;division&gt;/birdshot:&lt;major&gt;.&lt;minor&gt;.&lt;patch&gt; .</cite></p></li>
</ol>
<p>The <em>carta-integrations</em> and <em>pycarta</em> packages are pure python and, therefore,
are platform independent.</p>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>Step 4 can be used for custom container. Just replace the command above with:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="n">docker</span> <span class="n">buildx</span> <span class="n">build</span> <span class="o">--</span><span class="n">platform</span> <span class="n">linux</span><span class="o">/</span><span class="n">x86_64</span> <span class="o">-</span><span class="n">t</span> <span class="n">images</span><span class="o">.</span><span class="n">sbgenomics</span><span class="o">.</span><span class="n">com</span><span class="o">/</span><span class="n">htmdec</span><span class="o">/&lt;</span><span class="n">IMAGE</span> <span class="n">NAME</span><span class="o">&gt;</span><span class="p">:</span><span class="o">&lt;</span><span class="n">IMAGE</span> <span class="n">VERSION</span><span class="o">&gt;</span> <span class="o">.</span>
</pre></div>
</div>
</div>
</section>
<section id="uploading-your-docker-container-to-seven-bridges">
<h3>Uploading Your Docker Container to Seven Bridges<a class="headerlink" href="#uploading-your-docker-container-to-seven-bridges" title="Link to this heading">¶</a></h3>
<p>To push the newly built docker container into Seven Bridges and make it
available for building Apps requires some one-time setup. You must first login
to the Seven Bridges repo. For this you will need your Seven Bridges API key.</p>
<ol class="arabic simple">
<li><p>Log in to <a class="reference external" href="https://igor.sbgenomics.com/home">Seven Bridges</a></p></li>
<li><p>From the header bar, select <em>Developer</em> &gt;&gt; <em>Authentication token</em>. If you don’t have a token, click <em>Generate Token</em>.</p></li>
<li><p>Copy the token. You’ll need that for the next step: <code class="docutils literal notranslate"><span class="pre">docker</span> <span class="pre">login</span></code>.</p></li>
</ol>
<p>From a terminal on your local computer–e.g., Terminal on POSIX/MacOS or
PowerShell on Windows:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="n">docker</span> <span class="n">login</span> <span class="n">images</span><span class="o">.</span><span class="n">sbgenomics</span><span class="o">.</span><span class="n">com</span><span class="o">/&lt;</span><span class="n">division</span><span class="o">&gt;</span> <span class="o">-</span><span class="n">u</span> <span class="o">&lt;</span><span class="n">YOUR</span> <span class="n">USERNAME</span><span class="o">&gt;</span>
</pre></div>
</div>
<p>You will then be prompted for your password. This is the API token you copied
above. (Alternatively you can provide the token on the <code class="docutils literal notranslate"><span class="pre">docker</span> <span class="pre">login</span></code>
command line with the <code class="docutils literal notranslate"><span class="pre">-p</span> <span class="pre">&lt;API</span> <span class="pre">KEY&gt;</span></code> option, but then your API key will be
visible in your terminal history. Generally this should be avoided.)</p>
<section id="push">
<h4>Push<a class="headerlink" href="#push" title="Link to this heading">¶</a></h4>
<p>Having logged in you can now push your docker image to Seven Bridges:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="n">docker</span> <span class="n">push</span> <span class="n">images</span><span class="o">.</span><span class="n">sbgenomics</span><span class="o">.</span><span class="n">com</span><span class="o">/&lt;</span><span class="n">division</span><span class="o">&gt;/&lt;</span><span class="n">IMAGE</span> <span class="n">NAME</span><span class="o">&gt;</span><span class="p">:</span><span class="o">&lt;</span><span class="n">IMAGE</span> <span class="n">VERSION</span><span class="o">&gt;</span>
</pre></div>
</div>
</section>
</section>
</section>
<section id="usage">
<h2>Usage<a class="headerlink" href="#usage" title="Link to this heading">¶</a></h2>
<p>This package relies on two sets of credentials that should be maintained in
confidence:</p>
<p><em>Carta username/password</em> If not specified, the environment will be searched
for CARTA_USER (username) and CARTA_PASS (password).</p>
</section>
<section id="note">
<h2>Note<a class="headerlink" href="#note" title="Link to this heading">¶</a></h2>
<p>This project has been set up using PyScaffold 4.5. For details and usage
information on PyScaffold see <a class="reference external" href="https://pyscaffold.org/">https://pyscaffold.org/</a>.</p>
</section>
</section>


          </div>
          
        </div>
      </div>
      <div class="sphinxsidebar" role="navigation" aria-label="main navigation">
        <div class="sphinxsidebarwrapper">
<h1 class="logo"><a href="index.html">birdshot</a></h1>








<h3>Navigation</h3>
<ul class="current">
<li class="toctree-l1 current"><a class="current reference internal" href="#">Overview</a><ul>
<li class="toctree-l2"><a class="reference internal" href="#installation">Installation</a></li>
<li class="toctree-l2"><a class="reference internal" href="#usage">Usage</a></li>
<li class="toctree-l2"><a class="reference internal" href="#note">Note</a></li>
</ul>
</li>
<li class="toctree-l1"><a class="reference internal" href="cli.html">CLI</a></li>
<li class="toctree-l1"><a class="reference internal" href="contributing.html">Contributions &amp; Help</a></li>
<li class="toctree-l1"><a class="reference internal" href="license.html">License</a></li>
<li class="toctree-l1"><a class="reference internal" href="authors.html">Authors</a></li>
<li class="toctree-l1"><a class="reference internal" href="changelog.html">Changelog</a></li>
<li class="toctree-l1"><a class="reference internal" href="api/modules.html">Module Reference</a></li>
</ul>

<div class="relations">
<h3>Related Topics</h3>
<ul>
  <li><a href="index.html">Documentation overview</a><ul>
      <li>Previous: <a href="index.html" title="previous chapter">birdshot</a></li>
      <li>Next: <a href="cli.html" title="next chapter">birdshot</a></li>
  </ul></li>
</ul>
</div>
<div id="searchbox" style="display: none" role="search">
  <h3 id="searchlabel">Quick search</h3>
    <div class="searchformwrapper">
    <form class="search" action="search.html" method="get">
      <input type="text" name="q" aria-labelledby="searchlabel" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"/>
      <input type="submit" value="Go" />
    </form>
    </div>
</div>
<script>document.getElementById('searchbox').style.display = "block"</script>








        </div>
      </div>
      <div class="clearer"></div>
    </div>
    <div class="footer">
      &copy;2023, Contextualize LLC.
      
      |
      Powered by <a href="http://sphinx-doc.org/">Sphinx 7.2.6</a>
      &amp; <a href="https://github.com/bitprophet/alabaster">Alabaster 0.7.13</a>
      
      |
      <a href="_sources/readme.rst.txt"
          rel="nofollow">Page source</a>
    </div>

    

    
  </body>
</html>