======
pandas
======

This is the documentation of **pandas**. This package is a CLI wrapper around
python's `pandas` package, making complex loading, manipulation, and operations
available on the CLI for use as a tool on the Seven Bridges platform.

These functions act much like a posix pipe or to the imagemagick suite of tools.
Commands are strung together to pass objects from one command to the next. For
example::

    pandas read foo.xlsx tabular -f pretty

will read the file "foo.xlsx" and write its contents in a pretty format to the
stdout.

.. note::

    Many of the functions available on the CLI affected using the `-p/--params`
    option. This is a pipe ("|")-separated list of `<key>=<value>` pairs that are
    passed to the underlying pandas function. This gives you a very high level of
    control over the functionality of these command line tools. 


Contents
========

.. toctree::
   :maxdepth: 2

   Overview <readme>
   Reference <pandas>
   Contributions & Help <contributing>
   License <license>
   Authors <authors>
   Changelog <changelog>
.. Module Reference <api/modules>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. _toctree: https://www.sphinx-doc.org/en/master/usage/restructuredtext/directives.html
.. _reStructuredText: https://www.sphinx-doc.org/en/master/usage/restructuredtext/basics.html
.. _references: https://www.sphinx-doc.org/en/stable/markup/inline.html
.. _Python domain syntax: https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
.. _Sphinx: https://www.sphinx-doc.org/
.. _Python: https://docs.python.org/
.. _Numpy: https://numpy.org/doc/stable
.. _SciPy: https://docs.scipy.org/doc/scipy/reference/
.. _matplotlib: https://matplotlib.org/contents.html#
.. _Pandas: https://pandas.pydata.org/pandas-docs/stable
.. _Scikit-Learn: https://scikit-learn.org/stable
.. _autodoc: https://www.sphinx-doc.org/en/master/ext/autodoc.html
.. _Google style: https://google.github.io/styleguide/pyguide.html#38-comments-and-docstrings
.. _NumPy style: https://numpydoc.readthedocs.io/en/latest/format.html
.. _classical style: https://www.sphinx-doc.org/en/master/domains.html#info-field-lists
