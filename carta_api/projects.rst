.. _carta-api-projects-description:

projects
========

``{Carta Base URL}/project-api/v1``

The project API provides the functionality that is needed for a creation, retrieval, updating, and
saving forms in Collections, ``POST``-ing the JSON form payload. This includes serving the collection
HTML page, which is constructed based on a form's JSON schema and, in the case of an existing form,
the form's JSON data. 

“Project” and “Connection” resources are Carta Resources, in that they are integrated with the
:ref:`Carta Permission <carta-api-permissions-description>` system, and in that they may be useful
constructs for other, future Carta applications. (This is somewhat contrary to the typical microservices
architecture, where each service owns their own resources, and manages permissions to it.) 
In order to not leak implementation detail (e.g. the Collections UI simply needs to know the connection ID and name 
of a backend connection, and not the path to where the backend secret is stored), API endpoints for fetching projects 
and connections are included in the project API, and the Collection UI always queries the project and carta object 
storage v2 API through the Carta API, rather than querying the Carta API directly. 

A path mapper abstraction was also put in place. For a particular collection project, a simple interface has been defined 
that derives a form's path given the project and form data. Assuming a fairly simple form-data-to-path mapping, implementing 
a new path mapper for a project should not take more than a couple of hours of development time. 

Endpoint used to set up Collection projects and connections
------------------------------------------------------------

Endpoint: ``{BASE_URL}/service/carta/group/projects``

**Example**
    
.. code-block:: python

    response = requests.post(
    url=f"{BASE_URL}/project",
    headers={"Authorization": api_token},
    json = {
        "name" : "projectname"
        "bucketName": "carta-sandbox-projectname"
    })

