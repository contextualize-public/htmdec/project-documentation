.. _carta-api-service-description:

service
=======

Much of the functionality in domain-specific applications is just that: domain specific. Such functionality
often represents significant investment by several stakeholders and, consequently, it is desireable that
such functionality is available to as many users as possible.

While not strictly necessary, Services work closely with Dispatchers in the Carta ecosystem to expose
such functionality.

Services provide an abstraction between a backend compute resource, such as Seven Bridges, AWS Lambda, GCP
Cloud Functions, or Azure Functions, and the end application. This allows the compute resource to move in
response to technical and organization restructuring without breaking the connection to the federated,
decentralized set of applications that rely on that service.

Of course, this opens a potential security breach, as external users access an internal resource. However,
when the functionality that resource provides is combined with :ref:`Carta Permissions <carta-api-permissions-description>`,
this solution is more attractive than the widespread dissemination of the algorithm or source code. Firstly,
the user of the function need not be an expert in that functionality, but need only understand the appropriate
use of the output: a much lower burden. Secondly, algorithm development in a research context is iterative.
An API-first development ensures all users are using the latest version of the algorithm. Finally, by placing
this functionality behind the Carta authentication/authorization service, the permission set to access the
function is separate from the permission to access any additional resources--such as a database or other
data resource. This provides an explicit acknowledgement of the difference between access to an algorithm
and access to specific data.

The ``/service/{namespace}/{service}/{routeParams}`` endpoint allows HTTP GET, POST, PATCH, PUT and DELETE requests 
to be forwarded to another service. A simple route resolver class has been implemented that either uses the base 
URL configured for the service, or a mapping of namespace and service to a domain name. For example, users could
open a nginx/gunicorn/flask server to act as the API server and point a Carta Service endpoint to this or any
other microservice.

Register the ``Organization`` namespace
---------------------------------------

To avoid namespace conflicts, Carta services are subdivided into namespaces. The following shows how a user
might reserve the ``htmdec`` namespace. Operations developed under the umbrella of this organization or project
would be placed within this namespace.

.. code-block:: python

    response = requests.post(
    url=f"{BASE_URL}/service/reserve/htmdec",
    headers={"Authorization": api_token}
    )

Register the ``foo`` service
----------------------------

The following example shows how one might register an AWS API Gateway proxy to an AWS Lambda microservice.
When such a system is created, the API is accessed through a publicly-accessible ``baseurl``.

.. code-block:: python

    response = requests.post(
    url=f"{BASE_URL}/service/register/htmdec/foo?baseurl=https://xxxxx.execute-api.us-east-2.amazonaws.com/prod",
    headers={"Authorization": api_token}
)
