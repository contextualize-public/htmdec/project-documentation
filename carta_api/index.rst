Carta API
=========

A C# web server that is an internet-accessible gateway, routing traffic to other Carta services. 
In addition to being a gateway, the other main responsibilities of the Carta API are to:

- Check that users are authenticated
- Manage permissions for user-defined user and group access and use of resources
  - Active projects, e.g., BIRDSHOT
  - Backend file stores, e.g., Google Drive,
  - Services (see next bullet)
- Unified API access to external services, e.g. Seven Bridges

While not built to satisfy the requirements of the HTMDEC project, the Carta API was designed to
allow for project- and program-specific customization built: a capability leveraged in building
the other tools and components provided to HTMDEC participants, specifically:

- :ref:`Collections <applications-collections-description>`
- :ref:`DiGraph <applications-digraph-description>`
- :ref:`Dispatchers <carta-dispatch-birdshot-dispatcher-description>`

Carta provides a number of services intended to abstract away the details of the underlying
project resource. This simplfies user interaction with these resources and provides flexibility
to the resource provider to change implementation-specific details of the resource without
breaking promises made, implicitly or explicitly, with downstream dependents.


.. toctree::
   :maxdepth: 2

   auth
   service
   user
   files
   permissions
   projects
   secrets
