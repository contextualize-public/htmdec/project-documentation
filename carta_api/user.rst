.. _carta-api-user-description:

user
====

A set of endpoints that can be used to query Carta users and user groups (GET /user endpoints), 
and to create new users and user groups (POST /user endpoints). The latter requires that the 
Carta user creating the new user or user group have the necessary service permissions.

Getting user information
------------------------

.. code-block:: python

    def createUserAccount(username, email, tenant, api_token):

    response = requests.get(
        url=f"{BASE_URL}/user",
        headers={"Authorization": api_token}
    )
    print(f"User Response {response.status_code}: {response.reason}")
    pprint(json.loads(response.text))

The output from this call provides information on the current authenticated user.

.. code-block:: json

    {
        "id": "string",
        "name": "string",
        "email": "string",
        "firstName": "string",
        "lastName": "string",
        "organization": "string",
        "groups": [
            "string"
        ]
    }


Only specially designated administrative Carta users have been granted permission to create
new users or groups. For those select few, the following is an example of how a new user
can be created.

Example to create a new user
----------------------------

.. code-block:: python

    def createUserAccount(username, email, tenant, api_token):

    response = requests.post(
        url=f"{BASE_URL}/user",
        headers={"Authorization": api_token},
        json = {
            "name" : username,
            "email": email,
            "organization": tenant
        }
    )
    print(f"User Response {response.status_code}: {response.reason}")
    pprint(json.loads(response.text))
    
    response = requests.post(
        url=f"{BASE_URL}/user/group/AllUsers/{username}",
        headers={"Authorization": api_token}
    )
    print(f"AllUsers Group Response {response.status_code}: {response.reason}")
    
    response = requests.post(
        url=f"{BASE_URL}/user/group/{tenant}:All/{username}",
        headers={"Authorization": api_token}
    )
    print(f"{tenant}:All Group Response {response.status_code}: {response.reason}")
