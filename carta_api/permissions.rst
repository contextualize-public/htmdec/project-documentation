.. _carta-api-permissions-description:

permissions
===========

.. image:: permissions.png
   :align: center

Permission control what Resources (groups, data backends, services, files, etc.) a user or
group (collectively, a User) can access and what that User is allowed to do with that
Resource.

While critical, users will rarely be concerned with creating resource permissions. They will,
instead, set permissions to allow other Users and Groups to access those Resources to which
they have administrator access.

Users vs Resource
-----------------

No explicit restrictions have been imposed on what type of entity can be a User and what 
type of entity can be a Resource. In practice, however, some entities will only ever be a
User, and others will only be a Resource, and still others, such as groups, can be both.
This construction allows Users to access Resources through more than one path.

Bitflag
-------

Borrowing from, but extending, Unix file permissions, permissions are represented by a 5-bit flag:

- ``a``: admin
- ``c``: clone
- ``r``: read
- ``w``: write
- ``x``: execute

Notably, the ``admin`` and ``clone`` permissions allow a User to control permission assignment to
others or to duplicate a Resource, respectively. The latter is currently reserved, and is intended
for Users to create a deep copy of a Resource--one that the User can modify without impacting the
original Resource.


Implementation Detail
----------------------

A new Carta user is added through the Carta API ``POST /user`` endpoint, creating a vertex in the underlying graph
database with that User's user ID. When the User creates a new Carta Resource through the Carta API (e.g., 
for a file upload with ``POST files/Carta``), a file entity node is created in the graph database with a UUID, and 
a permissions edge is added from the User node to the file (Resource) node. File metadata with that same UUID is also
created in a NoSQL DB to hold information about the data backend that hosts the file. Together, this GraphDB and
NoSQL DB form the Carta Persistence Abstractions (CPA).

The User then has the option of sharing that resource with another user, user group, or Carta Resource through 
the Carta API ``PUT /permissions/{resourceId}/{userId}`` endpoint. The Carta API uses the Carta Resources resource 
manager to accomplish this. The resource manager, in turn, uses the CPA.

Endpoints can be used to:
-------------------------

**Find the resources a user has access to:**

* ``GET /Permissions/resources/{resourceType}``

**Query the permissions of a particular resource:**

* ``GET /Permissions/{id}``
* ``GET /Permissions/{id}/{userId}``
* ``GET /Permissions/{id}/users``

**Manage permissions**

* ``PUT /Permissions/{id}/{userId}``
* ``DELETE /Permissions/{id}/{userId}``
