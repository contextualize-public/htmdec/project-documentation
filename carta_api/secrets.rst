.. _carta-api-secrets-description:

secrets
=======

A user can use the ``PUT /secrets`` and ``GET /secrets`` endpoints to securely store secrets 
that are specific to themselves (no other Carta user or Contextualize person can retrieve
a User's secrets). A specific AWS IAM configuration has been put in place that retrieves a secret
through the User's Cognito identity token.

Put secret
-----------

In the following example, a user stores a data resource token.

.. code-block:: python

    htToken = getpass("Enter database token: ")
    secretName = "my-data-resource"
    response = requests.put(
        url=f"{BASE_URL}/secrets",
        headers={"Authorization": api_token,
                f"secret-{secretName}": htToken}
    )
    print(f"Response {response.status_code}: {response.reason}")

Expiration time
---------------

Explicitly set the expiration time in minutes for a secret. The default is 24 hours (24 |times| 60 minutes).
In the following example, the secret expires after one hour.

.. |times| unicode:: U+000d7

.. code-block:: python

    response = requests.put(
        url=f"{BASE_URL}/secrets",
        headers={"Authorization": api_token,
                f"secret-{secretName}": htToken},  
        params = {"expirationTime" : 60}
    )
    print(f"Response {response.status_code}: {response.reason}")

Get the secret
--------------

Secrets are retrieved by name but a list of those names cannot
be queried. **Remember your secret names; there is no way to recover them!**

.. code-block:: python

    response = requests.get(
        url=f"{BASE_URL}/secrets",
        headers={"Authorization": api_token},  
        params = {"name" : secretName}
    )
    print(f"Response {response.status_code}: {response.reason}")
