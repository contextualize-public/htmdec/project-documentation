.. _carta-api-files-description:

files
=====

Endpoints used to upload, update, fetch and delete user owned files (``/files/{source}`` endpoints).
These endpoints connect to project-specific connections to one-or-more data backends. By default,
these data are persisted on an AWS S3 bucket provisioned for the project during setup.

Upload a file
------------

Create a test file

.. code-block:: python

    # Create a test file
    test_file = open("test_file.txt", "w")
    test_file.writelines("My Test File")
    test_file.close()

Upload the file

.. code-block:: python

    # Upload the file
    upload_file = open("test_file.txt", "rb")
    file_stream = {'file':("/MyDir/MySubDir/create_test.txt",upload_file.read())}
    response = requests.post(
        url=f"{BASE_URL}/files/Carta",
        files=file_stream,
        headers={"Authorization": api_token},    
        stream=True
    )
    upload_file.close()
    print(f"Response {response.status_code}: {response.reason}")
    pprint(json.loads(response.text))


List all files a user has access to
-------------------------------------

Retrieve a list of all files a user has access to

.. code-block:: python

    response = requests.get(
        url=f"{BASE_URL}/files/Carta/list",
        params = { "path" : "/"},
        headers={"Authorization": api_token}
    )
    print(f"Response {response.status_code}: {response.reason}")
    pprint(json.loads(response.text))

Download a file
---------------

Download a file by its ID

.. code-block:: python

    file_id = json.loads(response.text)[-1]['id']
    print(file_id)
    response = requests.get(
        url=f"{BASE_URL}/files/Carta/file/{file_id}",
        headers={"Authorization": api_token}
    )
    print(f"Response {response.status_code}")
    print(response.content)

Update a file
-------------

Create an updated file and upload it to replace the existing file

.. code-block:: python

    # Create an updated file
    test_file = open("updated_test_file.txt", "w")
    test_file.writelines("My Updated Test File")
    test_file.close()

    # Upload the file
    upload_file = open("updated_test_file.txt", "rb")
    file_stream = {'file':("/MyDir/MySubDir/updated_test_file.txt",upload_file.read())}
    response = requests.patch(
        url=f"{BASE_URL}/files/Carta/file/{file_id}",
        files=file_stream,
        headers={"Authorization": api_token},    
        stream=True
    )
    upload_file.close()
    print(f"Response {response.status_code}: {response.reason}")
    pprint(json.loads(response.text))

    # Verify the update
    response = requests.get(
        url=f"{BASE_URL}/files/Carta/file/{file_id}",
        headers={"Authorization": api_token}
    )
    print(f"Response {response.status_code}")
    print(response.content)

Delete a file
-------------

Delete a file by its ID

.. code-block:: python

    response = requests.delete(
        url=f"{BASE_URL}/files/Carta/file/{file_id}",
        headers={"Authorization": api_token}
    )
    print(f"Response {response.status_code}: {response.reason}")
