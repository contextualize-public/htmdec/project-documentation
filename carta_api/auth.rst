.. _carta-api-auth-description:

auth
====

The ``auth`` endpoint returns information needed to setup an authenticated session using
AWS Cognito. This is one of the very few carta endpoints that may be queried without prior
authentication. It is the responsibility of the developer to setup an AWS SRP (Secure
Remote Password) connection to securely authenticate the user against the user pool
information provided by this endpoint. That is, Carta uses Cognito as an authentication
provider.

The following code block shows an example use case for the ``auth`` endpoint in the
python programming language.

.. code-block:: python

    def getCartaApiToken(username, password):
        response = requests.get(
            url=f"{BASE_URL}/auth"
        )
        userPool = json.loads(response.content)
        poolId = userPool["userPoolId"]
        region = userPool["region"]
        clientId = userPool["userPoolWebClientId"]

        client = boto3.client("cognito-idp", region_name=region)
        aws = AWSSRP(username=username, 
                     password=password, 
                     pool_id=poolId,
                     client_id=clientId, 
                     client=client)
        tokens = aws.authenticate_user()
        return "Bearer {}".format(tokens['AuthenticationResult']['IdToken'])
