.. _applications-collections-description:

Collections
===========

The Collections App is used to collect data and metadata relevant to a materials 
research project through the use of a consistent schema, in this case, JSON Schema
(c.f. :ref:`Data Definitions <background-data-definitions>`). The intended audience for the
Collections App includes those generating data, typically students, postdocs, and
researchers in a laboratory setting.

The use of JSON schema to capture equipment-relevant information enables several key 
functionalities:
    1. guiding researchers on the information that needs to be collected, as predetermined by a Subject Matter Expert (SME),
    2. providing error validation and checking, and
    3. enforcing structure on the uploaded data.

The user interface for the Collections App is intentionally simple and intuitive. 
It displays the schema name to allow a user to submit information for a new sample
or to navigate to edit an existing form.

Usage
-----

On initial access, the user is presented with a username and password challenge.

.. image:: ../resources/collections-login.png

Their response is tested against registered Carta users, and if found, the user is 
presented with a list of projects that the user can access.

.. image:: ../resources/collections-projects.png

Once a project is selected, the relevant project information such as structure and existing 
forms are retrieved from the connected data store, and the user is presented with an interactive 
tree-view of their structured data. From here, a user can choose to navigate the structure to 
an existing form or start a new form.

.. image:: ../resources/collections-project-home.png

A more in-depth tutorial for using Collections is available `in our Quick Start Guides <quick_start_guides-collections-description>`_.

JSON Schema
-----------

The use of JSON Schema is described on the :ref:`Data Definitions <background-data-definitions>`
page. The following pages show the schema developed for and in collaboration with the BIRDSHOT
team to support their two campaigns.

BIRDSHOT Campaign 1
^^^^^^^^^^^^^^^^^^^

`DED Details <../package-html/birdshot-schema/campaign-1/ded-details-v1/index.html>`_

`DED Traveler <../package-html/birdshot-schema/campaign-1/ded-traveler-v1/index.html>`_

`EDS/EBSD <../package-html/birdshot-schema/campaign-1/eds-ebsd-details-v2/index.html>`_

`LIPIT <../package-html/birdshot-schema/campaign-1/lipit-details-v1/index.html>`_

`Microhardness Details <../package-html/birdshot-schema/campaign-1/microhardness-details-v1/index.html>`_

`Microindentation Fiducials <../package-html/birdshot-schema/campaign-1/microindentation-fiducials-details-v1/index.html>`_

`Mounting/Polishing (TAMU) <../package-html/birdshot-schema/campaign-1/tamu-mounting-polishing-details-v1/index.html>`_

`Nanoindentation/HSR <../package-html/birdshot-schema/campaign-1/ni-hsr-details-v1/index.html>`_

`Nanoindentation/Microindentation <../package-html/birdshot-schema/campaign-1/ni-imicro-details-v1/index.html>`_

`Nanoindentation/Strain-rate Jump Test (SRJT) <../package-html/birdshot-schema/campaign-1/ni-srjt-details-v2/index.html>`_

`Porosity Imaging <../package-html/birdshot-schema/campaign-1/porosity-imaging-details-v1/index.html>`_

`Small Punch Test <../package-html/birdshot-schema/campaign-1/spt-details-v1/index.html>`_

`Tensile <../package-html/birdshot-schema/campaign-1/tensile-details-v2/index.html>`_

`VAM Processing <../package-html/birdshot-schema/campaign-1/vam-processing-details-v2/index.html>`_

`VAM Synthesis <../package-html/birdshot-schema/campaign-1/vam-synthesis-details-v2/index.html>`_

`VAM Traveler <../package-html/birdshot-schema/campaign-1/vam-traveler-v2/index.html>`_

`X-ray Diffraction (XRD) <../package-html/birdshot-schema/campaign-1/xrd-details-v2/index.html>`_

BIRDSHOT Campaign 2
^^^^^^^^^^^^^^^^^^^

`DED Details <../package-html/birdshot-schema/campaign-2/ded-details-v1/index.html>`_

`EDS/EBSD <../package-html/birdshot-schema/campaign-2/eds-ebsd-details-v1/index.html>`_

`LIPIT <../package-html/birdshot-schema/campaign-2/lipit-details-v1/index.html>`_

`Microhardness <../package-html/birdshot-schema/campaign-2/microhardness-details-v1/index.html>`_

`Nanoindentation/Strain-rate Jump Test (SRJT) <../package-html/birdshot-schema/campaign-2/ni-srjt-details-v1/index.html>`_

`Nanoindentation/HSR <../package-html/birdshot-schema/campaign-2/ni-hsr-details-v1/index.html>`_

`SHPB Compression <../package-html/birdshot-schema/campaign-2/shpb-compression-details-v1/index.html>`_

`Small Punch Test (SPT) <../package-html/birdshot-schema/campaign-2/spt-details-v1/index.html>`_

`Tensile <../package-html/birdshot-schema/campaign-2/tensile-details-v1/index.html>`_

`VAM Processing <../package-html/birdshot-schema/campaign-2/vam-processing-details-v1/index.html>`_

`VAM Synthesis <../package-html/birdshot-schema/campaign-2/vam-synthesis-details-v1/index.html>`_

`X-ray Diffraction (XRD) <../package-html/birdshot-schema/campaign-2/xrd-details-v1/index.html>`_

Conclusions
-----------

The Collections app serves as a useful tool for materials research projects. The utilization 
of JSON schema not only guides researchers in systematically collecting pertinent information 
but also ensures error validation and structured data enforcement. The use of JSON schema also 
allows for some flexibility as opposed to a predetermined, general schema designed for the 
larger materials community.

The user-friendly interface displaying the underlying JSON schema facilitates straightforward 
submission and editing of data without functioning as a data store. This maintains the philosophy 
that an organization can select their preferred data store(s), ensuring data ownership, and 
providing the flexibility to switch to new data stores or structures while maintaining a familiar 
interface for data uploads.
