# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'HTMDEC'
copyright = '2023, Contextualize LLC'
author = 'Contextualize LLC'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx_rtd_theme',
    'sphinxcontrib.tikz'
]

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']


# -- Options for Latex processing --------------------------------------------
tikz_proc_suite = 'ImageMagick'
# tikz_latex_preamble = r'\usepackage[linguistics]{forest}'
latex_elements = {
'preamble': r'\usepackage{tikz}'
}


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

# html_theme = 'alabaster'
# html_theme = 'sphinx_material'
# html_theme = 'groundwork'
html_theme = 'sphinx_pdj_theme'
html_static_path = ['_static']

