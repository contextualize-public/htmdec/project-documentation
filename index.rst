.. HTMDEC documentation master file, created by
   sphinx-quickstart on Thu Dec 21 03:08:53 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Contextualize Documentation in Support of the HTMDEC Project
============================================================

There are many data-specific challenges that arise with materials research intiaitives, 
especially in the context of multi-instiutional collaborations such as the HTMDEC program.
An effective data management strategy is key in creating Findable, Accessibile, 
Interoperable, and Reusable data. Contextualize aims to capture the knowledge of domain
experts and create a solution that adapts to each individual workflow with ease.
Contextualize's goals are as follows:

- Our customers decide the architecture--structures (SQL, NoSQL, file storage, etc.), schemas, and data stores--that works best for their workflows.
- Our customers are free to use multiple data stores, as their work requires.
- We build a reversible map to our customers' data structures to standardize and simplify access and analysis.
- Users contributed based on their expertise; not *all* users are expected to code.
- Our customers maintain ownership and control of their data. We neither serve as a data store nor become data custodians.

Usability, Reusability, and fitness for purpose from the customer's perspective are
at the forefront of our design.

Building Documentation
======================

Install dependencies in requirements.txt, e.g., `pip install -r requirements.txt`. You may wish to do this in a virtual environment.

Documentation is built using the `make` utility. To build on a POSIX system: `make html` will build the documentation as an HTML document. Other options are available. `make` or `make help` will list the available options. Other build options may depend on additional packages, such as LaTeX.

.. toctree::
   :maxdepth: 4

   quick_start_guides/index
   background/index
   applications/index
   carta_compute/index
   carta_api/index
   carta_dispatch/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
