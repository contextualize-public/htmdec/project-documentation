Carta Compute
=============

Data is just a prerequisite to data-driven High Throughput Materials Discover under Extreme
Conditions; a necessary but no sufficient first step. The second is the development of
analysis algorithms that convert the raw, as-collected data into meaningful
properties/features.

This second step is exponentially more difficult than data collection and combinatorially
larger as analyses are connected to form workflows.

Therefore, to meaningfully advance the field of high throughput materials research requires
two separate, but equally important capabilities:

1. Enable an ecosystem for rapid, single-shot prototyping, deployment, reuse, and dissemination of analysis tools,
2. Expand access and use of these analysis tools to researchers in a low-code/no-code environment.

Seven Bridges
-------------
Partnering with Seven Bridges enabled users to package and share code or workflows in a low-code/no-code
environment. The platform facilitates collaboration among researchers by providing a centralized space for 
code-based analysis. Data can be accessed through Carta, maintaining a layer of data security and also providing
cloud-based computing. Seven Bridges also emphasizes reproducibility in analyses through sharing of workflows and 
pipelines that, once developed, can be run by even the most novice of programmers.  

Contextualize developed a number of Seven Bridges tools and workflows in support of the BIRDSHOT team. 
These were exported as CWL, and their underlying Docker containers were packaged as .tar files. This CWL and
packaged Docker information is available within the larger HTMDEC data package. 

Please explore the `Seven Bridges Knowledge Center <https://docs.sevenbridges.com/>`_ to learn more.

.. note::
  - Twenty-one HTMDEC participants were onboarded and trained on the Seven Bridges platform.
  - Eighteen tools and workflows were developed, averaging one-per-month.
  - Approximately 150 permutations of these 18 Workflows were produced by HTMDEC participants.
  - Custom tools were developed by one researcher after a single two-hour introduction to the platform.
  - All 200+ Workflows produced across all projects are independently customizable.
  - All 200+ Workflows are accessible through the Seven Bridges API.
  - Compute developed on Seven Bridges is accessible to external applications (python, web apps, etc.) as Carta services.
  - :ref:`Carta Services <carta-api-service-description>` are accessible only to authenticated and authorized users.

Tools
-----

yprofile
~~~~~~~~
The "yprofile" tool is designed for data profiling using the ydata-profiling python package. It generates an 
HTML profile of the input data complete with statistics and visualizations.

- **Inputs:**
  - A File representing the input data for profiling.
- **Outputs:**
  - A File with an HTML profile of the input data, identified by the file extension ".html".
- **Docker:**
  - ydata-profiling

birdshot-graph-download
~~~~~~~~~~~~~~~~~~~~~~~
The "birdshot-graph-download" tool facilitates the download of a BIRDSHOT graph using program
credentials protected through Carta authentication/authorization.

- **Inputs:**
  - username: Carta Username (string).
  - password: Carta Password (string).
  - use_development_env: Development Environment Flag (boolean, default: false).
  - no_auth: Disable Authentication Flag (boolean, default: null).
  - unpack: Unpack Metadata Flag (boolean, default: true).
- **Outputs:**
  - A File containing the downloaded BIRDSHOT graph, identified by the file extension "birdshot*.pkl".
- **Docker:**
  - birdshot

birdshot-graph-propagate
~~~~~~~~~~~~~~~~~~~~~~~~
The "birdshot-graph-propagate" tool is designed to propagate properties within a BIRDSHOT graph.
(See the :ref:`DiGraph Application <applications-digraph-description>` for a description of the
practical applications of propagation and aggregation.)

- **Inputs:**
  - graph: A File representing the BIRDSHOT graph to be processed.
  - algorithm: Traversal Algorithm (enum: "preorder", "nearest", "postorder", "scatter", default: "scatter").
  - method: Merge Method (enum: "skip", "append", default: "append").
  - start: Start Name for aggregation (string).
  - stop: Stop Name for aggregation (string).
- **Outputs:**
  - A File containing the propagated BIRDSHOT graph.
- **Docker:**
  - birdshot

birdshot-graph-aggregate
~~~~~~~~~~~~~~~~~~~~~~~~
The "birdshot-graph-aggregate" tool is designed to aggregate properties within a BIRDSHOT graph.

- **Inputs:**
  - graph: A File representing the BIRDSHOT graph to be processed.
  - algorithm: Traversal Algorithm (enum: "preorder", "nearest", "postorder", "scatter", default: "scatter").
  - method: Merge Method (enum: "skip", "append", default: "append").
  - start: Start Name for aggregation (string).
  - stop: Stop Name for aggregation (string).
- **Outputs:**
  - A File containing the propagated BIRDSHOT graph.
- **Docker:**
  - birdshot

birdshot-graph-reduce
~~~~~~~~~~~~~~~~~~~~~
The "birdshot-graph-reduce" tool is designed to perform reduction on properties within a BIRDSHOT graph.
When multiple properties collected during aggregation or propagation, the user will typically prefer to
perform a statistical analysis on the collected behavior, e.g. mean, standard deviation, etc. ``Reduce``
in this context retains its typical meaning: a function that accepts a list (or other iterable) and
returns smaller data, typically a scalar.

- **Inputs:**
  - graph: A File representing the BIRDSHOT graph to be processed.
  - func: Reduction function to use (enum: "central-tendency", "mean", "median", "mode", "std", "variance", "iqr", default: "scatter").
  - include: User-specified regular expression to include properties for reduction (string).
  - exclude: Regular expression describing properties to be excluded from reduction (string).
  - suffix: Whether to add a suffix based on the selected reduction function (boolean, default: false).
  - cast: Cast type (enum: "bool", "int", "float", "str").
  - default: Default value for the reduction (string).
  - replace: Flag to indicate if the reduction should replace existing properties (boolean).
- **Outputs:**
  - A File containing the reduced BIRDSHOT graph.
- **Docker:**
  - birdshot

birdshot-graph-to-csv
~~~~~~~~~~~~~~~~~~~~~
The "birdshot-graph-to-csv" tool is designed to convert a BIRDSHOT graph to a CSV file.
Data collected in a materials research context is fundamentally hierarchical, not tabular.
This function converts the hierarchically structured BIRDSHOT data into a CSV, a function
that is easily adaptable to other graph data. (These are typically not directly transferable
because each hierarchy represents a unique taxonomy using objects with differing ontologies.)

- **Inputs:**
  - A File representing the BIRDSHOT graph to be converted to CSV.
- **Outputs:**
  - A File containing the converted CSV file, with the filename pattern ".csv".
- **Docker:**
  - birdshot

birdshot-graph-to-json
~~~~~~~~~~~~~~~~~~~~~~
The "birdshot-graph-to-csv" tool is designed to convert a BIRDSHOT graph to a CSV file. As
with the CSV converter, it is often more useful to exchange data as a JSON-formatted object.
This function first converts to a table and then, from there, to JSON.

- **Inputs:**
  - A File representing the BIRDSHOT graph to be converted to JSON.
- **Outputs:**
  - A File containing the converted CSV file, with the filename pattern ".json".
- **Docker:**
  - birdshot

generate-random-trees
~~~~~~~~~~~~~~~~~~~~~
The "generate-random-trees" tool creates a forest of random trees with specified parameters such as the number of 
trees, branch factor, and depth. It generates both a JSON representation of the forest and an optional PNG image of the graph.
This function  proves useful for testing codes that expect a graph.

- **Inputs:**
  - num_trees (optional): An integer representing the number of trees in the forest.
  - branch_factor (optional): An integer representing the branch factor for tree generation.
  - depth (optional): An integer representing the depth of the trees in the forest.
  - save_image (optional): A boolean flag indicating whether to save an image of the graph.
  - API_KEY (optional): A string for providing an API key.
- **Outputs:**
  - json-graph: A File containing the generated forest in JSON format, with the filename pattern "*.json".
  - graph-image: A File containing an optional PNG image of the generated forest, with the filename pattern "*.png".
- **Docker:**
  - forest-of-trees

download-gdrive-file
~~~~~~~~~~~~~~~~~~~~
The "download-gdrive-file" tool downloads files from Google Drive using Birdshot. It requires the Google Drive 
file ID, username, and password for authentication. The ``username`` and ``password`` are Carta authentication
credentials. Only users who have been granted access to the BIRDSHOT Google Drive by the BIRDSHOT administrators
will be permitted to download files.

- **Inputs:**
  - graph: A File input representing the graph.
  - fileID: A File input with loadContents set to true, representing the Google Drive file ID.
  - USER: A string input representing the username for authentication.
  - PASS: A string input representing the password for authentication.
- **Outputs:**
  - A File output, representing the downloaded file.
- **Docker:**
  - birdshot

sed
~~~
The "sed" tool performs text transformations using the sed command-line utility. It takes a file input and a string 
command, applying the specified sed command to the input file.

- **Inputs:**
  - A File input representing the input file for the sed command.
  - command: A string input representing the sed command to be executed.
- **Outputs:**
  - A File output, representing the result of the sed command with the extension ".out".
- **Docker:**
  - jq-alpine

jq
~~~
The "jq" tool performs text transformations using the jq command-line utility. It takes a file input and a string 
command, applying the specified sed command to the input file.

- **Inputs:**
  - A File input representing the input file for the jq command.
  - command: A string input representing the jq command to be executed.
- **Outputs:**
  - A File output, representing the result of the sed command with the extension ".out".
- **Docker:**
  - jq-alpine

concat-xlsx
~~~~~~~~~~~
The "concat-xlsx" tool is a CommandLineTool designed to concatenate multiple Excel (.xlsx) files. It utilizes the 
read and concat commands in conjunction with the pandas library to read, concatenate, and write Excel files. Users 
can provide an array of input files.

- **Inputs:**
  - An array of File inputs representing the Excel files to be concatenated.
  - output_filename: A string input representing the desired filename for the concatenated output.
- **Outputs:**
  - A File output, representing the concatenated Excel file with the extension ".xlsx".
- **Docker:**
  - tools-pd

grab-property-value-from-json
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The "grab-property-value-from-json" tool is designed to extract a specific property value from a JSON file.

- **Inputs:**
  - File input representing the JSON file from which the property value will be extracted.
  - property: A string input representing the target property key to extract from the JSON.
- **Outputs:**
  - A File output, representing the extracted property value with the extension ".out".
- **Docker:**
  - grab-from-json

get-json-result-pairs
~~~~~~~~~~~~~~~~~~~~~
The "get-json-result-pairs" tool is designed to process a directed graph representing Google Drive data using 
the "birdshot" command. It identifies pairs of metadata (JSON) files and result files (e.g., CSV, Excel) based 
on specified regular expressions. Users can choose to return either the Google Drive file IDs or the file paths,
which is useful for debugging.

- **Inputs:**
  - graph: A File input representing the directed graph of Google Drive BIRDSHOT data.
  - json_pattern: A string input representing the regular expression for metadata (JSON) files (default: ".*.json").
  - result_pattern: A string input representing the regular expression for result files (default: ".*.xlsx").
  - return_path: A boolean input allowing users to choose between returning file IDs or file paths (default: null).
- **Outputs:**
  - json_result_pairs: A File output providing a list of file IDs or paths for metadata (JSON) and result files in a text file ("json-result-pairs.txt").
- **Docker:**
  - birdshot

Workflows
---------
birdshot-graph-context-dev
~~~~~~~~~~~~~~~~~~~~~~~~~~
Requires Username and Password. It downloads the birdshot dataset from Google Drive as a graph. 
This graph is (1) converted to json format, reshaped, and a node-link json file is created and (2) the leaf node properties 
are aggregated, reduced to their mean (if applicable), and propogated in a meaningful way, converted to csv, as well as fed 
to y-data profiling. This workflow is used by the Digraph app.

.. image:: birdshot-graph-context-dev.png

birdshot-graph-campaign1-context
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
An example of aggregating, reducing, and propogating properties in a meaningful way if you 
are aware of the underlying data structure. In this workflow, you can choose at which level in the hierarchical structure to 
start and stop aggregation in the birdshot dataset to produce a node link graph.

.. image:: birdshot-graph-campaign1-context.png

download-graph-and-get-json-result-pairs
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Requires Username and Password. This workflow will download the birdshot graph and 
find paired JSON metadata files and corresponding, user-specified, results files. In the default example, the workflow will search 
for all "ni-srjt" metadata files with an XLSX file available in the same folder (node).

.. image:: download-graph-and-get-json-result-pairs.png

birdshot-generate-ni-srjt-summary
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Requires Username and Password. This workflow expands on the 'download-graph-and-get-json-result-pairs'.
The workflow will use these paired files, download a JSON and its corresponding XLSX file containing results. From the JSON, it 
will read the error-checked and properly formatted Sample ID. From the results file, it will grab a user specified column of 
data (Hardness and Modulus), average each column, and store the averaged values in an xlsx file with the corresponding Sample ID. 
This workflow will continue through each sample so the result is 1 "Summary" spreadsheet for each Nanoindentation sample.

.. image:: birdshot-generate-ni-srjt-summary.png

.. toctree::
   :maxdepth: 2
