Carta Dispatcher
================

**Dispatchers** can be thought of as abstraction layers or promise keepers to build robust,
federated ecosystems that connect application development with R&D.

Traditionally, client applications are broken into two parts: a user interface (a.k.a., frontend)
and an API layer (backend). The frontend is responsible for all interaction with the user while
the backend handles the business logic of the application; provisions and manages backend-local
resources, such as a user database; and mediates access to external resources.

In an R&D environment, access to these resources, even their very location, may change for technical,
administrative, or logistical reasons. Such changes would break applications that depend on the
details of that resource. However, an even greater burden is that this pushes the onus onto the
application developers to maintain the connection to the backend resource. For widely used resources,
this could be tens, hundreds, even thousands of applications that must change to accommodate the new
resource details. As the functionality of the backend resource evolves and matures, that functionality
will likely be transferred from a generic platform to more bespoke, more performant systems.

Dispatchers, on the other hand, mediate that connection so that any change to the backend resource--
whether a change in its location/host (URL) or even in it's API--can be changed in one location: the
Dispatcher-Resource interface. The Dispatcher, then, maintains a consistent location and interface that
even developmental resources can expose to provide functionality to stakeholders.

Finally, and critically, Dispatchers incorporate authentication, authorization, project and file
management (e.g., CRUD operations) simply, removing the burden of constructing these critical
cybersecurity system from researchers.

**Authentication**

1. The client app initiates the authentication process by obtaining a Carta API authentication token through AWS Cognito for user authentication.
2. This authentication token is crucial for subsequent API requests, ensuring secure and authorized communication between the client app and the Carta API.

**Project and File Management**

3. After successful authentication, the client gains the ability to retrieve a list of available projects and files associated with the user within Carta.

**CRUD Operations**

4. The client app is equipped to perform a range of operations on files, including downloading, updating, and deleting files.

**User Permissions**

5. The system enforces user permissions, ensuring that actions such as creating, updating, or deleting schema files are executed based on the user's role and group memberships.
6. Permissions are granted and managed through the Carta API, with the client dispatcher facilitating these interactions.

Several Dispatchers were developed for the BIRDSHOT team, most notably for the :ref:`DiGraph Application <applications-digraph-description>`
