.. _background-access-controls:

Access Controls
===============

Effective access controls are fundamental to ensuring secure and well-managed data
environments. Access controls serve as the gatekeepers, regulating user interactions 
with data, workflows, and operations within the various platforms.

Data in materials research brings together several resources: from one-or-more
federated data stores, analysis tools and execution environments, to applications
from dashboards to modeling platforms. This means bringing together not just the
data, but respecting the authentication and authorization of each resource today,
tomorrow, and any time in the future when the user requests access.

User accounts on Carta are not given an implicit proxy to users' data. Rather than
becomes a trusted third party, Carta allows users to define services (e.g., servers
that define API endpoints) and control who is authorized to access and use those
services. Similarly, users' do not inherit permission to access a data resource
because they have access to a service or are part of a group. Secrets management
within Carta allows users to store sensitive information and explicitly make that
information available for an application to act on their behalf.

**Authentication** is a critical component of the access control framework,
ensuring the identity of users interacting with the platform. AWS Cognito plays a
pivotal role in this process, providing a robust and scalable authentication
service. With AWS Cognito, user authentication is seamlessly integrated,
offering secure login and sign-up functionalities. This authentication layer
safeguards HTMDEC by validating the identity of users and ensuring that only
authorized individuals access the platform.

**Authorization** governs the actions users can perform within HTMDEC. Permissions
can be granted by anyone with an `admin` role to that resource, whether to a resource
or a group of users. By default, any user who creates a resource is an administrator
of that resource.

While permission may be given to specific users on a users-by-user basis, this can become
onerous. Groups of users may be created. These groups may be granted the same permissions
as any user to access, modify, or administer a resource with one exception: only users may
create a resource. Members of a group inherit permissions granted to the group. Group
permissions augment a user's permissions. That is, if a user has admin access and a group
to which the user belongs has only read access, the user retains the more permissive
authority.

Because groups can be given access to any type of resource, groups can also be given
access to (equivalently, become members of) another group. That is, Group A becomes a
member of Group B. It is possible that Group A's permissions to Group B's resources are
more restrictive. That is, Group A is given read-only permission to Group B and its
resources. In that instance a member of Group A who accesses a resource, a resource to
which Group B has admin access, will still only be granted read-only access.
