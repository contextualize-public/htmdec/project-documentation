.. _background-data-definitions:

Data Definitions
================

A variety of data schemas have been developed for materials data, closely linked to their
associated databases. Adhering to a prescribed schema enhances the FAIR (Findable,
Accessible, Interoperable, and Reusable) aspects of data, but attempts to establish a universal
materials schema face currently insurmountable challenges due to technical and practical factors
that arise because research data is inherently incomplete. While permissive schemas allow
flexibility, they result in inconsistencies between and even within databases. In contrast,
prescriptive schemas offer structure but limit transferrability across materials subdomains.

Efforts to leverage FAIR attributes have driven initiatives in materials vocabularies,
semantics, and ontologies. Inspired by the semantic web, these endeavors aim to precisely
define materials data. Despite uncertainty about the effectiveness of this semantic approach,
the goal of schema, whether permissive or prescriptive, and ontologies is to bring structure to
materials data, facilitating interlinking and comparison. The BIRDSHOT center's materials
discovery illustrates the challenges of diverse subdomains, with each group defining a schema
tailored to their data. Compliance is ensured through a web application, Collections,
that simplifies data collection and, with schema that impose a custom, but predictable structure,
fosters interoperability and reusability.

While JSON Schema was chosen for its ubiquity and flexibility, the development of Collections
highlights the importance of compliance beyond schema creation. The web application ensures
data collection adheres to the defined schema, providing a user-friendly interface. Hosting
Collections as a public application allows multi-tenancy, enabling users involved in multiple
projects to switch seamlessly. This approach enhances compliance, completeness, and data
quality, fostering a more efficient and centralized workflow.

An example of one such schema for collecting X-ray diffraction metadata is provided below (a
more human-readable description of the elements in this schema can be found
`here <../package-html/birdshot-schema/campaign-2/xrd-details-v1/index.html>`_). Similar,
more readable description of each BIRDSHOT schema can be found in the description of
:ref:`Collections <applications-collections-description>`.

.. code-block:: javascript

    {
        "definitions": {
            "ucsd": {
            "$id": "#/definitions/ucsd",
            "properties": {
                "2\u03B8 Angle Start": {
                "type": "number",
                "propertyOrder": 0,
                "default": "0"
                },
                "2\u03B8 Angle End": {
                "type": "number",
                "propertyOrder": 1,
                "default": "80"
                },
                "Scan Speed": {
                "type": "number",
                "propertyOrder": 2,
                "description": "degrees/s",
                "default": "0.05"
                },
                "\u03C9 Angle": {
                "type": "array",
                "format": "table",
                "propertyOrder": 3,
                "items": {
                    "type": "object",
                    "properties": {
                    "\u03C9 Angle": {
                        "type": "number",
                        "description": "degrees"
                    }
                    }
                },
                "default": [
                    {
                    "\u03C9 Angle": -4
                    },
                    {
                    "\u03C9 Angle": -2
                    },
                    {
                    "\u03C9 Angle": 0
                    },
                    {
                    "\u03C9 Angle": 2
                    },
                    {
                    "\u03C9 Angle": 4
                    }
                ],
                "options": {
                    "compact": true
                }
                },
                "Notes": {
                "type": "string",
                "propertyOrder": 4
                },
                "Upload Directory": {
                "type": "info",
                "propertyOrder": 5,
                "title": "",
                "links": [
                    {
                    "rel": "Upload Directory",
                    "href": "https://drive.google.com/drive/folders/1ZmwelyUaKJnydPRK4Mt6pwFtmpLyL5UR?usp=drive_link"
                    }
                ]
                }
            }
            },
            "files": {
            "$id": "#/definitions/files",
            "properties": {
                "file": {
                "type": "string",
                "format": "url",
                "options": {
                    "upload": {
                    "upload_handler": "uploadHandler",
                    "auto_upload": true,
                    "allow_reupload": true
                    }
                },
                "links": [
                    {
                    "href": "https://drive.google.com/file/d/{{self}}/view?usp=share_link",
                    "rel": "view"
                    }
                ]
                }
            },
            "type": "object",
            "title": "file",
            "format": "grid"
            },
            "phases": {
            "$id": "/definitions/phases",
            "multiple": true,
            "properties": {
                "Structure": {
                "propertyOrder": 0,
                "type": "string"
                },
                "a": {
                "propertyOrder": 1,
                "type": "string"
                },
                "b": {
                "propertyOrder": 2,
                "type": "string"
                },
                "c": {
                "propertyOrder": 3,
                "type": "string"
                },
                "\u03B1": {
                "propertyOrder": 4,
                "type": "string"
                },
                "\u03B2": {
                "propertyOrder": 5,
                "type": "string"
                },
                "\u03B3": {
                "propertyOrder": 6,
                "type": "string"
                }
            },
            "title": "Phase Information",
            "type": "object",
            "format": "grid"
            },
            "date": {
            "$comment": "YYYY-MM-DD",
            "$id": "#/definitions/date",
            "errorMessage": "The format must be a date as YYYY-MM-DD",
            "format": "custom",
            "pattern": "^202[234]-((0[1-9])|(1[0-2]))-((0[1-9])|([12][0-9])|(3[0-1]))([ -](([01][0-9])|(2[0-3]))(:[0-5][0-9]){1,2})?$",
            "type": "string"
            }
        },
        "description": "Form to track the XRD characterization details.",
        "properties": {
            "path": {
            "type": "string",
            "default": "XRD",
            "options": {
                "hidden": true
            }
            },
            "batch": {
            "default": true,
            "options": {
                "hidden": true
            },
            "type": "boolean"
            },
            "Sample ID": {
            "propertyOrder": 0,
            "type": "array",
            "format": "table",
            "minItems": 1,
            "items": {
                "errorMessage": "Please enter a Sample ID such as BAA01_VAM-A_XRD or BAA01_DED-A_XRD",
                "format": "custom",
                "pattern": "(B[A-Z]{2}[0-9]{2})_(VAM|DED)-[A-Z]_XRD",
                "type": "string"
            }
            },
            "Start Date": {
            "$ref": "#/definitions/date",
            "propertyOrder": 1
            },
            "End Date": {
            "$ref": "#/definitions/date",
            "propertyOrder": 2
            },
            "Time Spent": {
            "description": "HH:MM",
            "errorMessage": "Please enter a time in the HH:MM format",
            "fieldType": "text",
            "format": "custom",
            "pattern": "^[0-9]+:[0-5][0-9]$",
            "propertyOrder": 4,
            "type": "string"
            },
            "Completed By": {
            "type": "string",
            "propertyOrder": 5
            },
            "XRD Process Overview": {
            "properties": {
                "XRD Model": {
                "type": "string",
                "propertyOrder": 0,
                "enum": [
                    "TAMU",
                    "Rigaku",
                    "Anton",
                    "Panalytical"
                ]
                },
                "X-ray Source": {
                "type": "string",
                "enum": [
                    "Cu K-\u03B1"
                ],
                "propertyOrder": 1
                },
                "Beam Voltage": {
                "default": "40",
                "description": "kV",
                "propertyOrder": 2,
                "type": "number"
                },
                "Beam Amperage": {
                "description": "mA",
                "propertyOrder": 3,
                "type": "number"
                },
                "Scan Type": {
                "type": "string",
                "enum": [
                    "\u03B8-2\u03B8",
                    "2\u03B8-\u03C9"
                ],
                "propertyOrder": 4
                },
                "TAMU Instrument Details": {
                "properties": {
                    "Frames": {
                    "default": "3",
                    "propertyOrder": 0,
                    "type": "number"
                    },
                    "Time per Frame": {
                    "default": "600",
                    "description": "s",
                    "propertyOrder": 1,
                    "type": "number"
                    },
                    "Theta": {
                    "default": "18.5",
                    "propertyOrder": 2,
                    "type": "number"
                    },
                    "Phi": {
                    "propertyOrder": 3,
                    "type": "number"
                    },
                    "Psi": {
                    "propertyOrder": 4,
                    "type": "number"
                    },
                    "Theta Shift per Frame": {
                    "default": "16.7",
                    "propertyOrder": 5,
                    "type": "number"
                    },
                    "Collimator": {
                    "default": "1",
                    "description": "mm",
                    "propertyOrder": 6,
                    "type": "number"
                    },
                    "Distance": {
                    "default": "19.96",
                    "description": "cm",
                    "propertyOrder": 7,
                    "type": "number"
                    },
                    "Scan Type (TAMU)": {
                    "default": "Coupled",
                    "propertyOrder": 8,
                    "type": "string"
                    },
                    "Scan Pattern": {
                    "default": "XY",
                    "propertyOrder": 9,
                    "type": "string"
                    },
                    "Results": {
                    "minItems": 1,
                    "items": {
                        "multiple": true,
                        "properties": {
                        "Sample ID": {
                            "type": "string",
                            "propertyOrder": 0,
                            "enumSource": "sampleid",
                            "watch": {
                            "sampleid": "root.Sample ID"
                            }
                        },
                        "Phase Information": {
                            "minItems": 1,
                            "items": {
                            "$ref": "#/definitions/phases"
                            },
                            "propertyOrder": 1,
                            "type": "array"
                        },
                        "Upload Directory": {
                            "type": "info",
                            "propertyOrder": 5,
                            "title": "",
                            "links": [
                            {
                                "rel": "Upload Directory",
                                "href": "https://drive.google.com/drive/folders/1ZmwelyUaKJnydPRK4Mt6pwFtmpLyL5UR?usp=drive_link"
                            }
                            ]
                        },          
                        "Notes": {
                            "type": "string",
                            "propertyOrder": 3
                        }
                        },
                        "title": "Sample ID",
                        "type": "object"
                    },
                    "propertyOrder": 10,
                    "type": "array",
                    "format": "table"
                    }
                },
                "options": {
                    "dependencies": {
                    "XRD Model": "TAMU"
                    }
                }
                },
                "UCSD Instrument Details": {
                "$ref": "#/definitions/ucsd",
                "options": {
                    "dependencies": {
                    "XRD Model": [
                        "Rigaku",
                        "Anton",
                        "Panalytical"
                    ]
                    }
                }
                }
            },
            "propertyOrder": 6,
            "type": "object",
            "format": "grid"
            }
        },
        "title": "XRD Details",
        "type": "object"
        }
