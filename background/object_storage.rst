.. _background-object-storage-definitions:

Object Storage
==============

The integration of code for data analysis reduces the need for researchers to initiate an analysis
at the completion of a test. While a minor barrier if the analysis is to be performed on the self-same
measurement, complex analysis pipelines requiring input from several measurements are ubiquitous in
research, and these benefit from automated execution as these measurements are completed. Centralizing
compute allows arbitrary analysis pipelines to be set up and triggered as data is collected.

Automated asynchronous data synthesis pipelines are common in materials research, enabling
the construction of composite performance metrics and iterative research pathways. This
approach allows for custom app development and domain-specific workflows, including ETL
pipelines for seamless data integration. Containerization using Docker facilitates deployment
and reproducibility of the data integration environment. This streamlined approach, as
illustrated in the figure below fosters a more efficient and centralized workflow for materials
research, promoting compliance, completeness, and data quality throughout the entire
data lifecycle.

The dependency network of data generators and consumers in materials research, represented 
metaphorically as on- and off-ramps in the figure below, emphasizes the need for a unified
approach. From raw material production to component-level testing, this comprehensive
framework ensures that data resources, whether private, shared, or public, are effectively
utilized, contributing to the advancement of materials science.

.. figure:: data-generation-off-ramps.png
   :align: center

   R&D is analogous to a freeway, with cars replaced with data. In R&D, data is
   generated continuously and the metaphorical off-ramps allow data to be pulled from the freeway
   and processed, only to continue onto another path or follow an on-ramp to be included in downstream
   analyses.

Within the BIRDSHOT workflow, various characterization techniques aim to optimize mechanical performance
using automation streamlining data synthesis pipelines. Quasi-static testing evaluates 
tensile characteristics, and bespoke analysis codes transform data for analysis, and automation ensures 
consistency and provides traceability, injecting metadata to enhance the interpretation of results.
The collaboration between Contextualize and the BIRDSHOT was just reaching a point of maturity where
analysis tools for specific measurement techniques were to be built, completing these automated
analysis pipelines.