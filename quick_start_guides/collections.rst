.. _quick_start_guides-collections-description:

Collections Quick Start Guide
=============================
Carta Collections separates the details of data storage while both enforcing data structure and
providing user aids that simplify compliant data collection. `Carta Collections <https://collection.contextualize.us.com/>`_
is currently hosted by Contextualize at https://collection.contextualize.us.com/.

Initial Setup
-------------

To obtain a user account, see :ref:`Carta API, User Section <carta-api-user-description>`
or contact customer.service@contextualize.us.com.

Using Collections
-----------------

Collections sports a highly intuitive user interface designed to allow users to
focus on data collection, not reacclimating themselves to project-specific differences
in the data storage, organization, and naming conventions.

At any time, you may return back to the 'Select Project' screen or Project Home Screen
by selecting 'Collection' or the Carta logo.

Access and sign-in
``````````````````
1. Navigate to `Carta Collections <https://collection.contextualize.us.com/>`_ via your desired browser.
   
   .. image:: ./fig/collections/collections-home.png
      :align: center

2. Click either of the two visible 'Sign In' links.
   
   .. image:: ./fig/collections/sign-in.png
      :align: center

3. Enter your username and password and hit 'Sign In'.
   
   .. image:: ./fig/collections/user-challenge.png
      :align: center

4. If prompted to 'Select Project', your username has permissions to view more than one project. If not, you will be sent directly to your available project.
   
   .. image:: ./fig/collections/select-project.png
      :align: center

   a. Click the arrow next to 'Select Project'.
   b. Select your desired project.

Project Home Page
`````````````````
   
   .. image:: ./fig/collections/project-home.png
      :align: center

   From the project home page you may edit existing data, begin a new form, export or
   view a metadata summary, or request help.

Edit Existing Data
''''''''''''''''''

1. Select the arrow next to your project name. This expands the hierarchical structure in which all data was saved.
2. Navigate through the structure by clicking the arrows or folder names to find the desired JSON form data to edit.

.. image:: ./fig/collections/birdshot-structure.png
   :align: center

3. Select the form.
4. Make desired changes by clicking a field and changing the text.
5. Hit 'Submit'.

.. image:: ./fig/collections/form-update.png
   :align: center

Start a New Form
''''''''''''''''

1. On the Project Home Screen, select 'Start New'.

.. image:: ./fig/collections/project-home.png
   :align: center

2. Select the desired new form.

.. image:: ./fig/collections/new-form.png
   :align: center

3. Populate all required and desired fields by typing in each field box.
4. If desired, upload a file.
   a. In the file upload section, select 'Browse'.
   b. Find the desired file to upload in the File Dialog.
   c. Select 'Upload'.
5. Hit 'Submit' at the bottom of the page. The JSON form and any uploaded files will be saved to the predetermined location.

Export All Project Metadata
'''''''''''''''''''''''''''

On the Project Home Page, select 'Export All'.

.. image:: ./fig/collections/project-home.png
   :align: center

A file dialog will open. Select the name and location where you would like this Export All folder to be saved. 
(Default: forms.xlsx in your system's default Downloads folder)

View a Summary of Uploaded Data
'''''''''''''''''''''''''''''''

1. Select 'Analysis'.

.. image:: ./fig/collections/project-home.png
   :align: center

2. Select 'Profile'.

.. note:: At this time, `ydata-profiling` is the only analysis available in Collection. See Seven Bridges Quick Start Guide for more data analysis and compute options.

3. Select the desired form metadata type of interest.

.. image:: ./fig/collections/profiling.png
   :align: center


Request Help
''''''''''''
Select 'Help' and a contact name. This will open your default email client and allow you to request help via email.

.. image:: ./fig/collections/help.png
   :align: center

If your default email browser is not configured, right-click on the hyperlink and select 'copy email address' to manually email the listed contacts.

*Note*: Each project 'Help' is customizable and may have information relevant to your work.

Sign Out
````````
Sign Out by selecting your username and then the 'Sign Out' button in the top right corner.

.. image:: ./fig/collections/sign-out.png
   :align: center
