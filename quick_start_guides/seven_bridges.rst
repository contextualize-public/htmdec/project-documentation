.. _quick_start_guides-seven-bridges-description:

Seven Bridges Quick Start Guide
===============================

Carta's compute engine is powered by `Seven Bridges <https://igor.sbgenomics.com/home>`_.
This platform allows users to collaboratively transition analytical methods
from ideation through prototyping, iteratively develop successful methods into
shareable tools, and build complex workflows with these tools.

Initial Setup
-------------

To set up your account and take the first steps:

- Create a project by following the instructions in the `Seven Bridges documentation <https://docs.sevenbridges.com/docs/create-a-project>`_.
- Explore the project dashboard as explained in the `Project Dashboard documentation <https://docs.sevenbridges.com/docs/project-dashboard>`_.

.. note:: For comprehensive information, refer to the `Seven Bridges Knowledge Center <https://docs.sevenbridges.com/>`_.

Upload a File from your Local System
-------------------------------------

1. From the project dashboard, select 'Files'.
2. Select 'Add Files'.
3. Select 'Your Computer'.
4. Drag and drop the file(s) into the window or select 'Browse File'.
5. When the upload is complete, select 'X' in the top right corner.

Run an Existing Analyses
------------------------

1. From the project dashboard, select 'Apps'.
2. Find the desired tool or workflow under 'Name'.
3. On the right-most column, select 'Run'.
4. Select any input files under 'Inputs'.
5. Fill out any required 'App Settings'.
6. Select 'Run' in the top right corner.

Add Carta Username and Password to Secrets Manager
--------------------------------------------------

An existing analysis may require authentication if accesses data through a Dispatcher.

1. From any Seven Bridges page, select 'Developer' on the top toolbar. Note: It may be best 
   to do this in a new tab as you will need to toggle between the Secrets Manager and the app 
   that needs access to your username and password.
2. Select 'Secret management'.
3. Select 'Add new secret'.
4. Enter the secret name and description. For example: use the name `CARTA_USER` and "Carta username." as the description.
5. Enter your Carta username in the "Secret" field.
6. 'Access level' and 'Type' can remain as the defaults of "Private" and "String" respectively.
7. Select 'Add Secret'.
8. Select 'Add new secret'.
9.  Enter the secret name and description. For example: use the name `CARTA_PASS`` and "Carta password." as the description.
10. Enter your Carta password in the "Secret" field.
11. 'Access level' and 'Type' can remain as the defaults of "Private" and "String" respectively.
12. Select 'Add Secret'.
13. On the right side, select the 3 dots.
14. Select 'Allow App'.
15. Enter the URL and revision number of the app that needs to access your secrets. This information 
    can be found on the homepage of the tool.
