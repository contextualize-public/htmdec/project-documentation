Quick Start Guides
==================

Carta Collections and Carta Compute (powered by Seven Bridges) address two
challenges facing materials research programs: capturing and structuring
the metadata that describes a process, technique, or measurement and capturing
the expertise needed to process the results from that step.

The following Quick Start Guides walk a user through data capture with
Collections and getting started with Seven Bridges.

.. toctree::
   :maxdepth: 2

   collections
   seven_bridges
